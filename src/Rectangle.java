/**
 * Created by Carlos on 3/8/2016.
 */
public class Rectangle {

    private Float base;
    private Float height;

    public Rectangle(Float height, Float base) {
        this.base = base;
        this.height = height;
    }

    //TODO:
    //getArea
	public Float getArea(){
		return base*height;
	}
    //TODO:
    //getPerimeter
	public Float getPerimeter() {
		return 2*base + 2*height;
	}

    public void shrink(Float factor){
        base = base*factor;
        height = height*factor;
    }
}
